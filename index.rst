.. |label| replace:: Zahlungskonditionen
.. |snippet| replace:: FvPaymentConditions
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.2.0
.. |maxVersion| replace:: 5.3.4
.. |version| replace:: 1.0.10
.. |php| replace:: 7.0


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Plugin zeigt im Account und in der Kasse die Office Line Zahlungskondition an, wenn der Kunde eine bestimmte Zahlungsart hinterlegt hat.



Frontend
--------
Wenn der Kunde die Zahlungsart die Konfiguriert ist hinterlegt hat wird zusätzlich die genaue Zahlungsart angezeigt. 
Der Kunde kann nun andere Shopware Zahlungsarten auswählen je nachdem was konfiguriert ist aber nicht innerhalb der genauen Zahlungsart wechseln.
Dieses Plugin wird benutzt um die OL Zahlungskonditionen im Shop darzustellen aber nur seine eigene.

.. image:: FvPaymentConditions2.png

.. image:: FvPaymentConditions3.png


Backend
-------
Konfiguration
_____________
.. image:: FvPaymentConditions1.png

:Auswahl der Zahlungsmethode: aktiviert bedeutet der Kunden kann zwischen die Shopware Zahlungsarten auswählen, wenn es deaktiviert ist, dann kann der Kunde nicht mehr die zahlungsart wechseln.
:Zahlungskondition: Hier wird das Freitext-Feld der genauen Beschreibung der Zahlungskondition zugeordnet.
:PaymentID: Hier wird die Shopware PaymentID hinterlegt die speziell für diese Funktion angelegt wurde. (nicht die Standard-Rechnung Zahlungsart nehmen)


technische Beschreibung
------------------------
keine

Modifizierte Template-Dateien
-----------------------------
:/account/index.tpl:
:/account/payment_fieldset.tpl:
:/checkout/change_payment.tpl:
:/checkout/confirm.tpl:


